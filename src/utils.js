import axios from "axios";
import { endpoint } from "./config"
import Swal from "sweetalert2";

const capitalizeChar = (c) => c.toUpperCase();

export function capitalizeFirst(s) {
    return s.replace(/^\w/, capitalizeChar)
}

export function capitalizeAll(s) {
    return s.split(" ").map(x => capitalizeFirst(x)).join(" ")
}

export async function checkAuthenticated() {
    try {
        await getInstance().get("/users/auth-check");
    } catch (e) {
        if (e.response && e.response.status > 401) {
            localStorage.removeItem("user");
            localStorage.removeItem("token");
        }
    }
}

export function getProductIcon(product) {
    const isDev = process.env.NODE_ENV === "development";
    return `${isDev ? "http://localhost:3000" : window.location.origin}/${product.icon.filepath.slice(7)}`
}

export function getInstance() {
    const isDevelopment = process.env.NODE_ENV === 'development';
    if (localStorage.token) {
        const headers = { Authorization: `Bearer ${localStorage.token}`};
        return isDevelopment ? axios.create({
            baseURL: "http://localhost:3000"+endpoint,
            headers
        }) : axios.create({
            baseURL: window.location.origin + endpoint,
            timeout: 1000,
            headers
        })
    }
    return isDevelopment ? axios.create({
        baseURL: "http://localhost:3000"+endpoint
    }) : axios.create({
        baseURL: window.location.origin + endpoint
    })
}

export async function displaySwalError(err) {
    console.error(err);
    if (!err.response) return;
    await Swal.fire(
        err.response.statusText,
        err.response.data.message,
        "error"
    );
}

export function costToString(cost) {
    if (!(cost % 1)) {
        return `${cost}.- CHF`
    }
    return `${cost.toFixed(2)} CHF`
};

export const instance = (process.env.NODE_ENV === "development") ? axios.create() : axios.create({
    baseURL: window.location.origin + endpoint,
    timeout: 1000,
});
