import React, { useState } from 'react';
import { Redirect } from "react-router-dom";
import './style.sass';
import {displaySwalError, getInstance} from "../utils";

import {Container, Form, Button} from "react-bootstrap";


function Login({setUser}) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    
    const [success, setSuccess] = useState(false);
    
    
    const handleChange = (setter) => {
        return (event) => {
            event.preventDefault();
            setter(event.target.value);
        }
    };
    
    const submitLogin = async (event) => {
        event.preventDefault();
        const body = {
            username: username.toLowerCase(),
            password
        };
        try {
            const { data: { token, user }} = await getInstance().post("/users/login", body);
            if (!user || !user.isAdmin) {
                return;
            }
            setUser(user);
            localStorage.setItem("user", JSON.stringify(user));
            localStorage.setItem("token", token);
            setSuccess(true);
        } catch (e) {
            await displaySwalError(e);
        }
    };

    if (success) {
        return <Redirect to={"/admin"}/>;
    }
    
    return (
        <Container className={"loginPage"}>
            <Form id={"loginForm"}>
                <h1>Admin Login</h1>
                <Form.Group controlId="formUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control onChange={handleChange(setUsername)} type="text" placeholder="Enter username" />
                </Form.Group>

                <Form.Group controlId="formPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control onChange={handleChange(setPassword)} isInvalid={false} type="password" placeholder="Password" />
                </Form.Group>
                
                <Button onClick={submitLogin} variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Container>
    );
}

export default Login;
