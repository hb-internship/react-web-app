import React, {useEffect, useRef, useState} from "react";
import {costToString, getInstance} from "../utils";
import Swal from "sweetalert2";

import BadgeScanner from "../Scanner";
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

export default function ShoppingCart({ order, setOrder }) {

    const cart = useRef();

    useEffect(() => {
        cart.current.scrollTop = cart.current.scrollHeight;
    }, [ order ]);
    
    const getOrderCost = () => {
        return order.reduce((t,x) => {
            return t + x.product.cost * x.count;
        }, 0);
    };
    
    const Item = ({ item }) => {
        const handleDelete = (event) => {
            event.preventDefault();
            setOrder((o) => {
                const copy = [].concat(o);
                const i = copy.findIndex((x) => x.product._id === item.product._id );
                if (i === -1) {
                    return copy;
                }
                copy[i].count--;
                if (!copy[i].count) {
                    copy.splice(i,1);
                }
                return copy;
            })
        };

        return <div className={"cart-item"}>
            <Button onClick={handleDelete} className={"delete-item"} variant={"light"} size={"sm"}><FontAwesomeIcon icon={faTimes} size={"lg"}/></Button>
            <span>{item.product.name}</span>
            <span>× {item.count}: </span>
            <span className={"cost"}>{(item.product.cost*item.count).toFixed(2)} CHF</span>
        </div>;
    };
    
    const TotalOrder = () => {
        if (!order.length) {
            return null;
        }
        return <div className={"total-order"}>
            <span>Total:</span>
            <span>
                {
                    costToString(getOrderCost())
                }
            </span>
        </div>;
    };
    
    const Buyout = () => {
        const [paying, setPaying] = useState(false);
        const [badgeId, setBadgeId] = useState("");
        const [loading, setLoading] = useState(false);
    
        useEffect(() => {
            if (!badgeId) {
                return;
            }
            (async () => {
                const body = {
                    order: order.map(x => ({ id: x.product._id, count: x.count })),
                    badgeId
                };

                try {
                    setLoading(true);
                    const { data: { cost, user }} = await getInstance().post(`/orders`, body);
                    setLoading(false);
                    setPaying(false);

                    const amount = (user.balance < 0 ?
                        `<h4 class='negative'>Amount due` :
                        `<h4>Credit left`) + `: <strong>${costToString(Math.abs(user.balance))}</strong></h4>`;
                    await Swal.fire({
                        title: "Hooray!",
                        html:   "Your order was successfully registered!"+
                                "<br/>"+
                                `<h4>Amount paid: <strong>${costToString(cost)}</strong></h4>`+
                                amount,
                        type: "success",
                        timer: 3500
                    });
                    setBadgeId("")
                    setOrder([]);
                } catch (e) {
                    setLoading(false);
                    await Swal.fire({
                        title: e.response.statusText,
                        text: e.response.data.message,
                        type: "error",
                        timer: 3500
                    });
                }
            })();
        }, [badgeId]);
        
        if (!order.length) {
            return null;
        }
        
        const beginPayment = async (event) => {
            event.preventDefault();
            setPaying(true);
        };
        
        return <div className={"buyout"}>
            <Button onClick={beginPayment} variant={"light"} onKeyPress={(e) => e.preventDefault()}>PAY</Button>
            <BadgeScanner show={paying} onHide={() => setPaying(false)} setBadgeId={setBadgeId} loading={loading}/>
        </div>;
    };
    
    return (
        <div className={"shopping-cart"}>
            <h3>Your Order</h3>
            <hr/>
            <div className={"products"} ref={cart}>
                {order.length ? order.map((x,i) => <Item item={x} key={i}/>) : <div className={"empty-cart"}>No products selected...</div>}
            </div>
            <hr/>
            <TotalOrder/>
            <Buyout/>
        </div>
    );
}


