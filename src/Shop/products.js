import React, {useEffect, useState, Fragment} from "react";
import "./style.sass";
import {capitalizeAll, costToString, getInstance} from "../utils";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";


const ProductsList = React.memo(({ setOrder }) => {
    const [filter,setFilter] = useState("");
    const [categories, setCategories] = useState([]);
    const [products, setProducts] = useState([]);
    
    useEffect(() => {
        (async () => {
            try {
                const { data: { products: p }} = await getInstance().get("/products");
                const { data: { categories }} = await getInstance().get("/categories");
                setCategories(categories);
                setProducts(p);
            } catch (e) {
                console.error(e);
            }
        })();
    }, []);
    
    useEffect(() => {
        if (!filter) return;
        (async () => {
            try {
                const { data: { products: p }} = await getInstance().get("/products/category/"+filter);
                setProducts(p);
            } catch (e) {
                console.error(e);
            }
        })();
    }, [filter]);
    
    const Product = React.memo(({ product }) => {
        const { name, cost, icon } = product;
        
        const [animations, setAnimations] = useState([]);
        
        const handleOrder = (event) => {
            event.preventDefault();
            setAnimations(a => {
               const copy = [].concat(a);
               copy.push(Date.now());
               return copy;
            });
            setTimeout(() => {
                setAnimations(a => {
                    const copy = [].concat(a);
                    copy.shift();
                    return copy;
                })
            }, 600);
            setOrder(order => {
               const copy = [].concat(order);
               let prod = copy.find(x => x.product._id === product._id);
               if (!prod) {
                   prod = {
                       product,
                       count: 1
                   };
                   copy.push(prod);
               } else {
                   prod.count++;
               }
               return copy;
            });
        };
        
        return <div style={{zIndex: `${animations.length}`}} className={"product"} onClick={handleOrder}>
            <div className={"product-img"}>
                {icon && <Fragment>
                    <img src={`http://localhost:3000/${icon.filepath.slice(7)}`} alt={`${name} icon`}/>
                    {animations.map((x,i) => <img key={i} className={"alt animation"} src={`http://localhost:3000/${icon.filepath.slice(7)}`} alt={`${name} icon`}/>)}
                </Fragment>}
            </div>
            <div className={"product-info"}>
                <span className={"name"}>{name}</span>
                <span className={"cost"}>{costToString(cost)}</span>
            </div>
        </div>
    }, () => true);
    
    return (
        <div>
            {categories && <Tabs id="controlled-tab-example" activeKey={filter} onSelect={k => setFilter(k)}>
                {categories.map((x,i) => <Tab key={i} eventKey={x._id} title={capitalizeAll(x.name)}/>)}
            </Tabs>}
            <div className={"products-list"}>
                {products.map((x,i) => <Product product={x} key={i}/> )}
            </div>
        </div>
    );
}, () => true);

export default ProductsList;
