import React, {useState} from "react";
import {generateBreadcrumb} from "../Admin";
import ProductsList from "./products";
import ShoppingCart from "./cart";

export default function Shop() {
    const [order, setOrder] = useState([]);
    return (
        <div>
            {generateBreadcrumb()}
            <div className={"shop"}>
                <ProductsList setOrder={setOrder}/>
                <ShoppingCart setOrder={setOrder} order={order}/>
            </div>
        </div>
    );
}
