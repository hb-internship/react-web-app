import React, {useEffect} from "react";
import { Redirect } from "react-router-dom";

export default function Logout({setUser}) {
    useEffect(() => {
        localStorage.removeItem("user");
        localStorage.removeItem("token");
        setUser(null);
    }, [setUser]);
    
    return (
        <Redirect to={"/"}/>
    );
}
