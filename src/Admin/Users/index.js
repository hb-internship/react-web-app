import React, { useState, useEffect } from 'react';
import './style.sass';

import {generateBreadcrumb} from "../index";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Jumbotron from "react-bootstrap/Jumbotron";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Pagination from "react-bootstrap/Pagination";

import UsersTable from "./table";
import {getInstance} from "../../utils";
import UserCreationModal from "./create";
import UsersSearch from "./search";

export const userColors = {
    0: "",
    1: "warning",
    2: "primary",
    3: "danger"
};

export const getUserType = (user) => {
    if (user.isAdmin) return "admin";
    if (user.isGuest) return "guest";
    return "employee";
};

export const userTypes = {
    ALL: 0,
    GUEST: 1,
    EMPLOYEE: 2,
    ADMIN: 3,
    SEARCH: 4
};

export const modeTypes = {
    0: "all",
    1: "guest",
    2: "employee",
    3: "admin"
};

function Users() {
    const [page, setPage] = useState(0);
    const [pageSize, setPageSize] = useState(25);
    const [maxPages, setMaxPages] = useState(0);
    const [results, setResults] = useState({});
    const [mode, setMode] = useState(userTypes.ALL);
    const [currentTable, setCurrentTable] = useState(userTypes.ALL);
    
    useEffect(() => {
        (async () => {
            const { data: { pageSize, usersCount, guestCount, adminCount } } = await getInstance().get("/users/stats");
            setPageSize(pageSize);
            setResults({ users: usersCount, guests: guestCount, admins: adminCount, search: 0 });
        })();
    }, []);
    
    useEffect(() => {
        const { users, guests, admins } = results;
        let count;
        switch (currentTable) {
            case (userTypes.EMPLOYEE):
                count = users - guests - admins;
                break;
            case (userTypes.GUEST):
                count = guests;
                break;
            case (userTypes.ADMIN):
                count = admins;
                break;
            default:
                count = users;
                break;
        }
        setMaxPages(Math.ceil(count / pageSize));
    }, [currentTable, results, pageSize]);
    
    const handlePrevPage = () => {
        if (page === 0) {
            return;
        }
        setPage(p => p-1);
    };
    
    const handleNextPage = () => {
        if (page === maxPages - 1) {
            return;
        }
        setPage(p => p+1);
    };
    
    const handleSelect = (k) => {
        setCurrentTable(parseInt(k));
        setPage(0);
        setResults(r => Object.assign({}, r, { search: 0 }))
    };
    
    return (
        <div>
            {generateBreadcrumb()}
            <Container>
                <Jumbotron>
                    <h1>Users page</h1>
                    <span>There is a total of {results.users} users registered.</span>
                </Jumbotron>
                <ButtonToolbar className={"registrations"}>
                    <Button variant={"primary"} onClick={() => setMode(userTypes.EMPLOYEE)}>Register Employee</Button>
                    <Button variant={"warning"} onClick={() => setMode(userTypes.GUEST)}>Register Guest</Button>
                    <Button variant={"danger"} onClick={() => setMode(userTypes.ADMIN)}>Register Admin</Button>
                </ButtonToolbar>
                {currentTable < userTypes.SEARCH && <Pagination>
                    <Pagination.Prev disabled={page === 0} onClick={handlePrevPage} />
                    <Pagination.Item disabled>Page {page+1} of {maxPages}</Pagination.Item>
                    <Pagination.Next disabled={page === (maxPages - 1)} onClick={handleNextPage}/>
                </Pagination>}
                <Tabs onSelect={handleSelect} unmountOnExit={true} activeKey={currentTable} defaultActiveKey={userTypes.ALL} id="uncontrolled-tab-example">
                    <Tab eventKey={userTypes.ALL} title={`All (${results.users})`}>
                        <UsersTable page={page} type={userTypes.ALL}/>
                    </Tab>
                    <Tab eventKey={userTypes.EMPLOYEE} title={`Employees (${results.users - results.guests - results.admins})`}>
                        <UsersTable page={page} type={userTypes.EMPLOYEE}/>
                    </Tab>
                    <Tab eventKey={userTypes.GUEST} title={`Guests (${results.guests})`}>
                        <UsersTable page={page} type={userTypes.GUEST}/>
                    </Tab>
                    <Tab eventKey={userTypes.ADMIN} title={`Admins (${results.admins})`}>
                        <UsersTable page={page} type={userTypes.ADMIN}/>
                    </Tab>
                    <Tab eventKey={userTypes.SEARCH} title={`Search Users${results.search ? ` (${results.search})` : ""}`}>
                        <UsersSearch page={page} setResults={setResults}/>
                    </Tab>
                    
                </Tabs>
                <UserCreationModal show={mode !== userTypes.ALL} mode={mode} onHide={() => setMode(userTypes.ALL)}/>
            </Container>
        </div>
    );
}

export default Users;
