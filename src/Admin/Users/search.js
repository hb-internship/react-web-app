import Form from "react-bootstrap/Form";
import React, {useEffect, useState} from "react";
import {getInstance} from "../../utils";
import UsersTable from "./table";

let timer;

const UsersSearch = ({ setResults: parentSetResults }) => {
    const [results, setResults] = useState([]);
    const [query, setQuery] = useState("");
    
    const lastKeyTimeout = 1000;
    
    useEffect(() => {
        parentSetResults(r => Object.assign({}, r, { search: results.length }));
    }, [results, parentSetResults]);
    
    
    useEffect(() => {
        if (!query) {
            setResults([])
        }
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(async () => {
            try {
                const { data: { users }} = await getInstance().get("/users/search?q="+query);
                setResults(users);
            } catch (e) {
                console.error(e);
            }
        }, lastKeyTimeout);
    }, [query]);
    
    const handleChange = (e) => {
        setQuery(e.target.value.trim().toLowerCase());
    };
    
    const handlePress = (e) => {
        e.persist();
        if (e.charCode === 13) {
            e.preventDefault();
        }
    };
    
    return <Form>
        <Form.Group controlId="formUserSearch">
            <Form.Control onKeyPress={handlePress} onChange={handleChange} type="text" placeholder="Search for user..." />
        </Form.Group>
        <UsersTable results={query ? results : []}/>
    </Form>
};

export default UsersSearch;
