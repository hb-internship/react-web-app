import React, {useCallback, useEffect, useState} from "react";

import User from "./user";
import {getInstance} from "../../utils";
import {modeTypes} from "./index";

const UsersTable = ({ type, page, results }) => {
    const [users, setUsers] = useState(results ? results : []);
    const [sortProp, setSortProp] = useState([]);
    
    const generateQueryURL = useCallback(() => {
        const [prop, order] = sortProp;
        let url = `/users/group/${modeTypes[type]}?page=${page}`;
        if (!order) {
            return url;
        }
        url += `&sort=${order > 0 ? "+" : "-"}${prop}`;
        return url;
    }, [page, sortProp, type]);
    
    
    useEffect(() => {
        if (results) {
            const copy = [].concat(results);
            const [prop,order] = sortProp;
            copy.sort((a,b) => {
                if (order > 0) {
                    if (a[prop] > b[prop]) {
                        return -1;
                    }
                    if (a[prop] < b[prop]) {
                        return 1;
                    }
                } else if (order < 0) {
                    if (a[prop] > b[prop]) {
                        return 1;
                    }
                    if (a[prop] < b[prop]) {
                        return -1;
                    }
                }
                return 0;
            });
            setUsers(copy);
        }
    }, [results,sortProp]);
    
    
    useEffect(() => {
        if (results) {
            return;
        }
        (async () => {
            const { data: { users }} = await getInstance().get(generateQueryURL());
            if (users && users.length > 0) {
                setUsers(users);
            }
        })();
    }, [generateQueryURL, page, results, sortProp]);
    
    const Header = ({ prop, title, order }) => {
        
        const changeOrder = (o) => {
            switch (o) {
                case 0: return 1;
                case 1: return -1;
                default: return 0;
            }
        };
        
        const handleClick = async (e) => {
            e.preventDefault();
            setSortProp([prop, changeOrder(order)]);
        };
        
        if (!prop || !title) {
            return <span/>
        }
        
        return <span className={["sortable", order > 0 ? "asc" : null, order < 0 ? "desc" : null].join(" ")} onClick={handleClick}>
            {title}
            <span className={"dir"}>▲</span>
        </span>
    };
    
    const headers = [
        {prop: "lastName", title: "Full name"},
        {prop: "username", title: "Username"},
        {prop: "balance", title: "Balance"},
        {}
    ];
    
    if (results && results.length === 0) {
        return null;
    }
    
    return <div className={["custom-table users-table", type].join(" ")}>
        <div className={"header"}>
            <div>
                {headers.map((x, i) => <Header key={i} order={sortProp[0] === x.prop ? sortProp[1] : 0} {...x}/>)}
            </div>
        </div>
        <div className={"body"}>
            {users.map((x,i) => <User key={x._id} user={x} idx={i} setUsers={setUsers}/>)}
        </div>
    </div>;
};

export default UsersTable;
