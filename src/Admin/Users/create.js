import React from "react";

import {capitalizeAll, getInstance} from "../../utils";
import Swal from "sweetalert2";

import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import {Button} from "react-bootstrap";

import { Formik } from "formik";
import * as yup from 'yup';
import {modeTypes, userColors, userTypes} from "./index";

const schema = yup.object({
    firstName: yup.string().max(40, "First name can't be longer than 40 characters").required("Please provide a first name").lowercase(),
    lastName: yup.string().max(40, "Last name can't be longer than 40 characters").required("Please provide a last name").lowercase(),
    email: yup.string().email("Please insert a valid email").required("Please enter an email").lowercase(),
    username: yup.string().min(6, "Username must be at least 6 characters long").max(20, "A username can't be longer than 20 characters").required("Please enter a username").lowercase(),
    password: yup.string().min(8, "Password must be at least 8 characters or longer").required("A password is required"),
    repeatPassword: yup.string().required("You have to repeat the password")
});

const UserCreationModal = (props) => {
    
    const handleSubmit = async (values, actions) => {
        const { email, username, password, repeatPassword } = values;
        if (password !== repeatPassword) {
            return actions.setFieldError("repeatPassword", "Must match the password");
        }
        const { data: resCheck } = await getInstance().get(`/users/check?email=${email.toLowerCase()}&username=${username.toLowerCase()}`);
        const keys = Object.keys(resCheck);
        for (let i = 0; i < keys.length; i++) {
            if (resCheck[keys[i]]) {
                actions.setFieldError(keys[i], "Already in use by another user");
            }
        }
        const invalid = keys.reduce((s,key) => s || resCheck[key], false);
        if (invalid) return;
        const body = Object.assign({}, values);
        delete body.repeatPassword;
        for (let key in body) {
            if (!body.hasOwnProperty(key) || key === "password") {
                continue
            }
            body[key] = body[key].toLowerCase();
        }
        if (props.mode === userTypes.GUEST) {
            body.isGuest = true;
        } else if (props.mode === userTypes.ADMIN) {
            body.isAdmin = true;
        }
        try {
            const { data: { user } } = await getInstance().post("/users", body);
            if (user) {
                Swal.fire(
                    'Successful!',
                    'User has been created and can now login',
                    'success'
                )
            }
            props.onHide();
        } catch (e) {
            Swal.fire(
                'Oops!',
                'Something went wrong while attempting to create a new user',
                'error'
            )
        }
    };

    return <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
    >
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                New {capitalizeAll(modeTypes[props.mode])}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Formik validationSchema={schema} onSubmit={handleSubmit} initialValues={{}}>
                {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    values,
                    touched,
                    isValid,
                    errors,
                }) => (
                    <Form noValidate onSubmit={handleSubmit}>
                        <Form.Row>
                            <Form.Group as={Col} md="6" controlId="firstName">
                                <Form.Label column={0}>First name</Form.Label>
                                <Form.Control
                                    name="firstName"
                                    type="text"
                                    placeholder="First name"
                                    value={values.firstName}
                                    onChange={handleChange}
                                    isValid={touched.firstName && !errors.firstName}
                                    isInvalid={!!errors.firstName}
                                />
                                <Form.Control.Feedback>First name is fine!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">{errors.firstName}</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="6" controlId="lastName">
                                <Form.Label column={0}>Last name</Form.Label>
                                <Form.Control
                                    name="lastName"
                                    type="text"
                                    placeholder="Last name"
                                    onChange={handleChange}
                                    value={values.lastName}
                                    isValid={touched.lastName && !errors.lastName}
                                    isInvalid={!!errors.lastName}
                                />
                                <Form.Control.Feedback>Last name is okay!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">{errors.lastName}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} md="6" controlId="email">
                                <Form.Label column={0}>Email</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="email"
                                    onChange={handleChange}
                                    placeholder="example@hugoboss.ch"
                                    value={values.email}
                                    isValid={touched.email && !errors.email}
                                    isInvalid={!!errors.email}
                                />
                                <Form.Control.Feedback>Email is okay!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    {errors.email}
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="6" controlId="username">
                                <Form.Label column={0}>Username</Form.Label>
                                <InputGroup>
                                    <Form.Control
                                        name={"username"}
                                        type="text"
                                        placeholder="Username"
                                        aria-describedby="inputGroupPrepend"
                                        onChange={handleChange}
                                        value={values.username}
                                        isInvalid={!!errors.username}
                                        isValid={touched.username && !errors.username}
                                    />
                                    <Form.Control.Feedback>Username is fine!</Form.Control.Feedback>
                                    <Form.Control.Feedback type="invalid">
                                        {errors.username}
                                    </Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} md="6" controlId="password">
                                <Form.Label column={0}>Password</Form.Label>
                                <Form.Control
                                    name="password"
                                    type="password"
                                    placeholder=""
                                    onChange={handleChange}
                                    value={values.password}
                                    isInvalid={!!errors.password}
                                    isValid={touched.password && !errors.password}
                                />
                                <Form.Control.Feedback>Password is valid!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    {errors.password}
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="6" controlId="repeatPassword">
                                <Form.Label column={0}>Confirm Password</Form.Label>
                                <Form.Control
                                    name="repeatPassword"
                                    type="password"
                                    placeholder=""
                                    onChange={handleChange}
                                    value={values.repeatPassword}
                                    isInvalid={!!errors.repeatPassword}
                                    isValid={touched.repeatPassword && !errors.repeatPassword}
                                />
                                <Form.Control.Feedback>Password is matching!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    {errors.repeatPassword}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>
                        <Button variant={userColors[props.mode]} type="submit">
                            Create {capitalizeAll(modeTypes[props.mode])}
                        </Button>
                    </Form>
                    
                )}
                </Formik>
        </Modal.Body>
    </Modal>
};

export default UserCreationModal;
