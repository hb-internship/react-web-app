import React, { useState } from "react";
import {costToString, getInstance, capitalizeAll} from "../../utils";
import {Button} from "react-bootstrap";

import Swal from "sweetalert2";

import {userColors, userTypes} from "./index";
import UserEditModal from "./edit";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const User = ({ user, setUsers }) => {
    const [editing, setEditing] = useState(false);
    
    const handleDelete = async () => {
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "This action will delete the currently selected user!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        });
        if (result.value) {
            try {
                const { data } = await getInstance().delete("/users/"+user._id);
                if (data.user) {
                    Swal.fire(
                        'Deleted!',
                        'User was successfully removed from the database',
                        'success'
                    );
                    setUsers(users => users.filter(x => x._id !== data.user._id))
                }
            } catch (e) {
                Swal.fire(
                    'Oh oh!',
                    'Something went wrong with the deletion of this user',
                    'error'
                )
            }
        }
    };
    
    const computeVariant = () => {
          if (user.isGuest) return userColors[userTypes.GUEST];
          if (user.isAdmin) return userColors[userTypes.ADMIN];
          return userColors[userTypes.EMPLOYEE];
    };
    
    return <div>
        <span>{capitalizeAll(user.fullName)}</span>
        <span>{user.username}</span>
        <span className={user.balance < 0 ? "negative" : ""}>{costToString(user.balance)}</span>
        <span className={"options"}>
            <Button variant={computeVariant()} onClick={() => setEditing(true)} size={"sm"}>Edit</Button>
            <Button variant={"secondary"} onClick={handleDelete} size={"sm"}><FontAwesomeIcon icon={faTimes} size={"lg"}/></Button>
            {/*<span className={["user-type", computeVariant()].join(" ")}/>*/}
        </span>
        <UserEditModal show={editing} user={user} setUsers={setUsers} onHide={() => setEditing(false)}/>
    </div>
};

export default User;
