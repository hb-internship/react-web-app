import React, {useEffect, useState} from "react";

import {capitalizeAll, getInstance} from "../../utils";
import Swal from "sweetalert2";

import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import {Button} from "react-bootstrap";
import {getUserType} from "./index";

import {Formik} from "formik";
import * as yup from 'yup';
import BadgeScanner from "../../Scanner";


const schema = yup.object({
    firstName: yup.string().max(40, "First name can't be longer than 40 characters").required("Please provide a first name").lowercase(),
    lastName: yup.string().max(40, "Last name can't be longer than 40 characters").required("Please provide a last name").lowercase(),
    balance: yup.number().required("Please provide a valid balance"),
    email: yup.string().email("Please insert a valid email").required("Please enter an email").lowercase(),
    username: yup.string().min(6, "Username must be at least 6 characters long").max(20, "A username can't be longer than 20 characters").required("Please enter a username").lowercase(),
});

const EditType = {
    MENU: 0,
    INFO: 1,
    PASSWORD: 2,
    BADGE: 3
};


const replaceUser = (user, setUsers) => {
    setUsers((users) => {
        const idx = users.findIndex(x => x._id === user._id);
        const copy = [].concat(users);
        copy[idx] = user;
        return copy;
    });
};

const UserEditModal = (props) => {
    const [mode, setMode] = useState(0);
    const [badgeId, setBadgeId] = useState("");
    const [loading, setLoading] = useState(false);
    
    const type = getUserType(props.user);
    const { user, setUsers } = props;
    
    useEffect(() => {
        setMode(0);
        setBadgeId("");
    }, [props.show]);
    
    
    
    useEffect(() => {
        (async () => {
            const { _id: id } = user;
            if (badgeId) {
                setLoading(true);
                try {
                    await getInstance().post(`/users/${id}/badge`, { badgeId });
                    Swal.fire(
                        "Success!",
                        "Badge has been successfully assigned to this guest",
                        "success"
                    );
                } catch (e) {
                    console.error(e);
                    Swal.fire(
                        'Oops!',
                        'Something went wrong while editing this user',
                        'error'
                    )
                } finally {
                    setLoading(false);
                }
                setMode(EditType.MENU);
            }
        })();
    }, [user, badgeId]);
    
    const makeAdmin = async () => {
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "This action will permanently make the selected user into an admin!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, go ahead!'
        });
        if (result.value) {
            try {
                const { data: { user: result }} = await getInstance().put("/users/"+user._id, { isAdmin: true });
                replaceUser(result, setUsers);
                if (result) {
                    Swal.fire(
                        'Successful!',
                        'Guest has been promoted to employee',
                        'success'
                    );
                    return props.onHide();
                }
            } catch (e) {
                Swal.fire(
                    'Oops!',
                    'Something went wrong while editing this user',
                    'error'
                )
            }
        }
    };
    
    const ModalMenu = () => {
        return <Row>
            <Col><Button onClick={() => setMode(EditType.INFO)}>Change Info</Button></Col>
            <Col><Button onClick={() => setMode(EditType.BADGE)}>Register Badge</Button></Col>
            {type !== "guest" && <Col><Button onClick={() => setMode(EditType.PASSWORD)}>Reset password</Button></Col>}
            {type === "employee" && <Col><Button onClick={makeAdmin}>Make Admin</Button></Col>}
        </Row>
    };
    
    const ModalInfo = (props) => {
        const handleSubmit = async (values, actions) => {
            const body = {};
            for (let key in values) {
                if (!values.hasOwnProperty(key)) continue;
                if (values[key] !== user[key]) {
                    body[key] = values[key];
                }
            }
            if (!Object.keys(body).length) {
                return props.onHide();
            }
            try {
                const { data: { user: result } } = await getInstance().put("/users/"+user._id, body);
                if (result) {
                    Swal.fire(
                        'Successful!',
                        'User information has been updated',
                        'success'
                    );
                    replaceUser(result, setUsers);
                }
                props.onHide();
            } catch (e) {
                Swal.fire(
                    'Oops!',
                    'Something went wrong while editing this user',
                    'error'
                )
            }
        };
        
        return <Formik validationSchema={schema} onSubmit={handleSubmit} initialValues={props.user}>
            {({
                  handleSubmit,
                  handleChange,
                  handleBlur,
                  values,
                  touched,
                  isValid,
                  errors,
              }) => (
                <Form noValidate onSubmit={handleSubmit}>
                    <Form.Row>
                        <Form.Group as={Col} md="4" controlId="validationFormik01">
                            <Form.Label>First name</Form.Label>
                            <Form.Control
                                type="text"
                                name="firstName"
                                value={values.firstName}
                                onChange={handleChange}
                                isValid={touched.firstName && !errors.firstName}
                            />
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationFormik02">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control
                                type="text"
                                name="lastName"
                                value={values.lastName}
                                onChange={handleChange}
                                isValid={touched.lastName && !errors.lastName}
                            />
                
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="validationFormik03">
                            <Form.Label>Balance (CHF)</Form.Label>
                            <Form.Control
                                type="number"
                                step={0.1}
                                name="balance"
                                value={values.balance}
                                onChange={handleChange}
                                isValid={touched.balance && !errors.balance}
                            />
                
                            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} md="8" controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="text"
                                name="email"
                                onChange={handleChange}
                                placeholder="example@hugoboss.ch"
                                value={values.email}
                                isValid={touched.email && !errors.email}
                                isInvalid={!!errors.email}
                            />
                            <Form.Control.Feedback>Email is okay!</Form.Control.Feedback>
                            <Form.Control.Feedback type="invalid">
                                {errors.email}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col} md="4" controlId="username">
                            <Form.Label>Username</Form.Label>
                            <InputGroup>
                                <Form.Control
                                    name={"username"}
                                    type="text"
                                    placeholder="Username"
                                    aria-describedby="inputGroupPrepend"
                                    onChange={handleChange}
                                    value={values.username}
                                    isInvalid={!!errors.username}
                                    isValid={touched.username && !errors.username}
                                />
                                <Form.Control.Feedback>Username is fine!</Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    {errors.username}
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                    </Form.Row>
                    <Button type="submit">Edit User</Button>
                </Form>
            )}
        </Formik>
    };
    
    const ModalReset = (props) => { return null };

    
    const BodyRenderer = () => {
        switch (mode) {
            case EditType.MENU: return <ModalMenu/>;
            case EditType.INFO: return <ModalInfo onHide={props.onHide} user={props.user}/>;
            case EditType.PASSWORD: return <ModalReset/>;
            default: return null;
        }
    };
    
    const renderTitle = () => {
        const title = capitalizeAll(type);
        switch (mode) {
            case EditType.MENU: return `Edit ${title}`;
            case EditType.INFO: return `Edit ${title} Info`;
            case EditType.PASSWORD: return `Resetting ${title} Password`;
            default: return "";
        }
    };
    
    if (mode === EditType.BADGE) {
        return <BadgeScanner show={mode === EditType.BADGE} onHide={() => setMode(EditType.MENU)} setBadgeId={setBadgeId} loading={loading}/>
    }
    
    return <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
    >
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                {mode !== EditType.MENU && <Button className={"back-icon"} size={"sm"} onClick={() => setMode(EditType.MENU)}>◀</Button>}{renderTitle()}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <BodyRenderer/>
        </Modal.Body>
    </Modal>
};

export default UserEditModal;
