import React  from "react";
import {costToString, capitalizeAll} from "../../utils";
import TimeAgo from "react-timeago/lib";
import { NavLink } from "react-router-dom";

const OrderRow = ({ order, idx, count, page, pageSize, user }) => {
    const d = new Date(order.createdAt);

    const countProducts = (order) => Object.values(order.order).reduce((p,x) => p+x, 0);
    const orderUser = typeof (order.user) === "object" ? order.user : user;
    return <div>
        <span>{count - (page*pageSize) - idx}</span>
        <span>{d.toLocaleDateString()}</span>
        <span>{d.toLocaleTimeString()}</span>
        <span><NavLink to={"/admin/orders/"+orderUser._id}>{capitalizeAll(orderUser.fullName)}</NavLink></span>
        <span>{countProducts(order)}</span>
        <span>{costToString(order.cost)}</span>
        <TimeAgo date={d}/>
    </div>
};

export default OrderRow;
