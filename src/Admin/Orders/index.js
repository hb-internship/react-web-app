import React, {useEffect, useState} from 'react';

import {generateBreadcrumb} from "../index";
import { Container, Jumbotron } from "react-bootstrap";
import OrdersTable from "./table";
import {capitalizeAll, displaySwalError, getInstance} from "../../utils";

const Orders = ({ match }) => {
    const [user, setUser] = useState(undefined);

    useEffect(() => {
        if (!match) {
            setUser(null);
            return;
        }
        const { userId } = match.params;
        (async () => {
            try {
                const { data: { user }} = await getInstance().get("/users/"+userId);
                setUser(user);
            } catch (e) {
                setUser(null);
                await displaySwalError(e);
            }
        })();
    }, [match]);

    return (
        <div>
            {generateBreadcrumb()}
            <Container>
                <Jumbotron>
                    <h1>Orders page</h1>
                    {user && <span>Currently displaying orders for: {capitalizeAll(user.fullName)}</span>}
                </Jumbotron>
                <OrdersTable user={user}/>
            </Container>
        </div>
    );
}

export default Orders;
