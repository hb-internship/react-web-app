/* eslint-disable react-hooks/exhaustive-deps */

import React, { useEffect, useState } from "react";
import {displaySwalError, getInstance} from "../../utils";
import OrderRow from "./row";

import "./style.sass";
import Pagination from "react-bootstrap/Pagination";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";

const getMonthDates = () => {
    const date = new Date();
    return {
        first: new Date(date.getFullYear(), date.getMonth(), 1, 12),
        last: new Date(date.getFullYear(), date.getMonth() + 1, 0, 12)
    };
};

const dateToInputString = (date) => {
    return date.toISOString().slice(0, 10);
};

const parseURL = (user, start, end, page) => {
    const route = user ? `/${user._id}` : "";
    const query = `?start=${dateToInputString(start)}&end=${dateToInputString(end)}&page=${page}`;
    return `/orders/all${route}${query}`;
};

const sendRequest = async (user, start, end, page, cb) => {
    try {
        const url = parseURL(user, start, end, page);
        const { data: { baskets, count, pageSize} } = await getInstance().get(url);
        cb.setPageSize(pageSize);
        cb.setCount(count);
        cb.setResults(baskets);
    } catch (e) {
        await displaySwalError(e);
    }
};

const OrdersTable = ({ user }) => {

    const [page, setPage] = useState(0);
    const [results,  setResults] = useState([]);
    const [count, setCount] = useState(0);
    const [pageSize, setPageSize] = useState(0);

    const [timer, setTimer] = useState(null);

    const [start, setStart] = useState(getMonthDates().first);
    const [end, setEnd] = useState(getMonthDates().last);

    useEffect(() => {
        const dates = getMonthDates();
        if (start.getTime() === dates.first.getTime() && end.getTime() === dates.last.getTime()) {
            return;
        }
        if (timer) {
            clearTimeout(timer);
        }
        setTimer( setTimeout(sendRequest, 500, user, start, end, page, { setPageSize, setCount, setResults }) );
    }, [start, end]);

    useEffect(() => {
        if (user === undefined) {
            return;
        }
        sendRequest(user, start, end, page, { setPageSize, setCount, setResults });
    }, [user, page]);

    useEffect(() => {
        setPage(0);
    }, [ user ]);

    const maxPages = Math.ceil(count / pageSize);

    const handleNextPage = () => {
        setPage(p => Math.min(p+1, maxPages));
    };

    const handlePrevPage = () => {
        setPage(p => Math.max(p-1, 0));
    };

    const handleStartChange = (e) => {
        if (e.target.valueAsDate > end) {
            return;
        }
        setStart(e.target.valueAsDate);
    }
    const handleEndChange = (e) => setEnd(e.target.valueAsDate);

    return <div>
        <Row>
            <Col md={8}>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            Start date
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl type={"date"} name={"start"} onChange={handleStartChange} value={dateToInputString(start)} id="basic-url" aria-describedby="basic-addon3" />
                    <InputGroup.Prepend className={"ml-3"}>
                        <InputGroup.Text>
                            End date
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl type={"date"} name={"end"} onChange={handleEndChange} value={dateToInputString(end)} id="basic-url" aria-describedby="basic-addon3" />
                </InputGroup>
            </Col>
            <Col md={4}>
                {maxPages > 1 && <Pagination>
                    <Pagination.Prev disabled={page === 0} onClick={handlePrevPage}/>
                    <Pagination.Item disabled>Page {page + 1} of {maxPages}</Pagination.Item>
                    <Pagination.Next disabled={page === (maxPages - 1)} onClick={handleNextPage}/>
                </Pagination>}
            </Col>
        </Row>
        <div className={"custom-table orders-table"}>
            <div className={"header"}>
                <div>
                    <span>#</span>
                    <span>Date</span>
                    <span>Time</span>
                    <span>Buyer</span>
                    <span>Items</span>
                    <span>Cost</span>
                    <span>Time ago</span>
                </div>
            </div>
            <div className={"body"}>
                {results.map((x, i) => <OrderRow key={x._id} idx={i} page={page} pageSize={pageSize} count={count} order={x} user={user}/>)}
            </div>
        </div>
    </div>;

};

export default OrdersTable;
