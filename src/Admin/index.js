import React, {useEffect} from 'react';
import './style.sass';
import { Switch, Route, Redirect } from "react-router-dom";
import UserPage from "./Users";
import ProductsPage from "./Products";
import CategoriesPage from "./Categories";
import OrdersPage from "./Orders";


import {Container, Jumbotron} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import {checkAuthenticated} from "../utils";

const AdminHome = () => {
    return <div className={"admin-home"}>
            {generateBreadcrumb()}
            <Container>
                <Jumbotron>
                    <h1>Admin page</h1>
                </Jumbotron>
                <div className={"custom-container"}>
                    <NavLink to={"/admin/categories"}><div><span>Categories</span></div></NavLink>
                    <NavLink to={"/admin/products"}><div><span>Products</span></div></NavLink>
                    <NavLink to={"/admin/users"}><div><span>Users</span></div></NavLink>
                    <NavLink to={"/admin/orders"}><div><span>Orders</span></div></NavLink>
                </div>
            </Container>
        </div>
};


export const generateBreadcrumb = () => {
    const subpaths = window.location.pathname.split("/");
    let url = "/";
    const results = [["/", "Home"]];
    for (let i = 0; i < subpaths.length; i++) {
        const subpath = subpaths[i];
        if (subpath.length === 0) {
            continue;
        }
        url+=subpath+"/";
        results.push([url, subpath.replace(/^\w/, (chr) => chr.toUpperCase())]);
    }
    
    const BreadcrumbItem = (props) => {
        const activeClass = props.active ? "active" : null;
        return <li className={["breadcrumb-item", activeClass].join(" ")}>
            {props.active ? <span className={"active text-muted"}>
                {props.text}
            </span> : <NavLink to={props.href}>{props.text}</NavLink>}
        </li>
    };
    return <div className={"breadcrumb-container"}>
        <Container>
            <Breadcrumb>
                {results.map((url, i) => <BreadcrumbItem key={i} active={i === results.length - 1} href={url[0]} text={url[1]}/>)}
            </Breadcrumb>
        </Container>
    </div>;
};

function Admin() {
    useEffect(() => {
        checkAuthenticated();
    }, []);
    
    if (localStorage.user && localStorage.token) {
    } else {
        return <Redirect to={"/login"}/>
    }
    
    return (
        <div>
            <Switch>
                <Route path={"/admin/products"}>
                    <ProductsPage/>
                </Route>
                <Route path={"/admin/categories"}>
                    <CategoriesPage/>
                </Route>
                <Route path={"/admin/users"}>
                    <UserPage/>
                </Route>
                <Route path={"/admin/orders/:userId"} component={({ match }) => <OrdersPage match={match}/>}/>
                <Route exact path={"/admin/orders"}>
                    <OrdersPage/>
                </Route>
                <Route>
                    <AdminHome/>
                </Route>
            </Switch>
        </div>
    );
}

export default Admin;
