import React, {useEffect, useState} from 'react';
import './style.sass';

import {generateBreadcrumb} from "../index";

import Swal from 'sweetalert2'


import {Row, Col, Form, Container, Jumbotron, Button} from "react-bootstrap";
import {capitalizeAll, displaySwalError, getInstance} from "../../utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const Categories = () => {
    
    const [categories, setCategories] = useState([]);
    
    const [editIdx, setEditIdx] = useState(-1);
    
    const Category = ({ category, idx }) => {
    
        const [name, setName] = useState(category.name);
        const beingEdited = editIdx === idx;
        
        const putCategory = async () => {
            if (name === category.name) {
                return;
            }
            try {
                const { data: result } = await getInstance().put("/categories/"+category._id, { name: name.toLowerCase() });
                setCategories(cats => {
                    const idx = cats.findIndex(x => x._id === result._id);
                    const results = [].concat(cats);
                    results[idx] = result;
                    return results;
                });
            } catch (e) {
                await displaySwalError(e);
            }
        };
    
        const handleClick = async (event) => {
            event.preventDefault();
            if (beingEdited) {
                setEditIdx(-1);
                return await putCategory();
            }
            setEditIdx(idx);
        };
        
        const handleChange = (event) => {
            setName(event.target.value);
        };
        
        
        const handleRemove = (event) => {
            event.preventDefault();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    getInstance().delete("/categories/"+category._id).then(res => {
                        const { data: category } = res;
                        setCategories(categories.filter(x => x._id !== category._id ));
                        Swal.fire(
                            'Deleted!',
                            'This category has been deleted.',
                            'success'
                        )
                    });
                }
            });
        };
        
        const categoryName = capitalizeAll(category.name);
        
        return <div>
            {beingEdited ? <input onChange={handleChange} type={"text"} defaultValue={categoryName}/> : <span>{categoryName}</span>}
            <span className={"options"}>
                <Button onClick={handleClick} size={"sm"}>{beingEdited? "Save" : "Edit" }</Button>
                <Button onClick={handleRemove} variant={"secondary"} size={"sm"}><FontAwesomeIcon icon={faTimes} size={"lg"}/></Button>
            </span>
        </div>
    };
    
    useEffect(() => {
        (async () => {
            try {
                const { data: { categories }} = await getInstance().get("/categories");
                setCategories(categories);
            } catch (e) {
                await displaySwalError(e);
            }
        })()
    }, []);
    
    const CategoryCreator = () => {
        const [name, setName] = useState("");
        const handleSubmit = (event) => {
            event.preventDefault();
            getInstance().post("/categories/new", { categoryName: name.toLowerCase() }).then((res) => {
                setCategories([res.data].concat(categories));
            });
        };
        
        return <Form.Group>
            <Row>
                <Col>
                    <Form.Control onChange={(e) => setName(e.target.value)} type={"text"} placeholder={"New category name"}/>
                </Col>
                <Col>
                    <Button onClick={handleSubmit}>Submit</Button>
                </Col>
            </Row>
        </Form.Group>
    };
    
    return (
        <div>
            {generateBreadcrumb()}
            <Container>
                <Jumbotron>
                    <h1>Categories page</h1>
                </Jumbotron>
               <CategoryCreator/>
                <div className={"custom-table categories-table"}>
                    <div className={"header"}>
                        <div>
                            <span>Name</span>
                            <span/>
                        </div>
                    </div>
                    <div className={"body"}>
                    {categories.map((x,i) => <Category key={x._id} idx={i} category={x}/>)}
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default Categories;
