import React, {useEffect, useState} from "react";
import {displaySwalError, getInstance} from "../../utils";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import {Button} from "react-bootstrap";

import IconPicker from "./icon"
import CategorySelector from "./select"
import Swal from "sweetalert2";

const CreationModal = ({ product, show, onHide, editing, setProducts }) => {
    const [name, setName] = useState(editing ? product.name : null);
    const [cost, setCost] = useState(editing ? product.cost : null);
    const [icon, setIcon] = useState(editing ? product.icon : null);
    const [categories, setCategories] = useState(product ? product.categories : null);

    useEffect(() => {
        if (!product) {
            setName(null);
            setCost(null);
            setIcon(null);
            setCategories(null);
        } else {
            setName(product.name);
            setCost(product.cost);
            setIcon(product.icon);
            setCategories(product.categories);
        }
    }, [show, product]);
    
    const compareValues = (a,b) => {
        if (typeof a !== typeof b) {
            return false;
        }
        if (Array.isArray(a) && Array.isArray(b)) {
            if (a.length !== b.length) {
                return false;
            }
            for (let i = 0; i < a.length; i++) {
                if (a[i] !== b[i]) {
                    return false;
                }
            }
            return true;
        }
        if (typeof a === "object" && typeof b === "object") {
            return a._id === b._id;
        }
        return a === b;
        
    };
    
    const computeDelta = () => {
        const p = product || {};
        const result = Object.assign({}, { name, icon, cost, categories });
        const keys = ["name", "cost", "icon", "categories"];
        for (let key of keys) {
            const inProduct = (key in p) && (compareValues(result[key], p[key]));
            if (!result[key] || inProduct) {
                delete result[key];
            }
        }
        return result;
    };

    const postProduct = (product) => {
        return getInstance().post("/products/new", product);
    };

    const putProduct = (product) => {
        return getInstance().put("/products/"+product._id, product);
    };

    const submitData = async () => {
        const payload = computeDelta();
        if (payload.categories) {
            payload.categories = payload.categories.map(x => x._id);
        }
        if (Object.keys(payload).length === 0) {
            return onHide();
        }
        const httpMethod = editing ? putProduct : postProduct;
        try {
            const { data: { product }} = await httpMethod(payload);
            setProducts(products => {
                const result = [].concat(products);
                if (editing) {
                    const idx = products.findIndex(x => x._id === product._id);
                    result[idx] = product;
                } else {
                    result.push(product);
                }
                return result;
            });
            onHide();
            await Swal.fire(
                'Success!',
                'The product was successfully '+ (editing ? 'edited' : 'created') +'!',
                'success'
            );
        } catch (e) {
            await displaySwalError(e);
        }
    };

    const handleCostChange = (e) => {
        const value = Math.round(parseFloat(e.target.value)*100)/100;
        setCost(value);
    };

    return <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
    >
        <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                { editing ? "Edit Product" : "New Product"}
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form id={"product-creation"}>
                <Form.Group controlId="productName">
                    <Form.Label column={0}>Name</Form.Label>
                    <Form.Control onChange={(e) => setName(e.target.value.trim())} defaultValue={editing ? name : null} name={"name"} type="text" placeholder="Insert a name for the new product" />
                </Form.Group>
                <Form.Group controlId="productCost">
                    <Form.Label column={0}>Cost</Form.Label>
                    <Form.Control onChange={handleCostChange} step={0.1} defaultValue={editing ? cost : null} name={"cost"} type="number" placeholder="A Cost for your product" />
                </Form.Group>
                <Form.Group id="productIcon">
                    <Form.Label column={0}>Icon</Form.Label>
                    <IconPicker iconId={icon ? icon._id : null} setIcon={setIcon}/>
                </Form.Group>

                <CategorySelector categories={categories} setCategories={setCategories}/>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>Cancel</Button>
            <Button variant="primary" onClick={submitData} type="submit">
                {editing ? "Edit" : "Create"}
            </Button>
        </Modal.Footer>
    </Modal>
};

export default CreationModal;
