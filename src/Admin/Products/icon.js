import React, {useEffect, useState} from "react";
import {displaySwalError, getInstance} from "../../utils";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";

const IconPicker = ({ iconId, setIcon }) => {
    const [selIcon, setSelIcon] = useState(-1);
    const [availIcons, setAvailIcons] = useState([]);
    const [isUploading, setUploading] = useState(false);

    useEffect(() => {
        setSelIcon(() => {
            let idx = -1;
            for (let i = 0; i < availIcons.length; ++i) {
                if (availIcons[i]._id === iconId) {
                    idx = i;
                    break;
                }
            }
            return idx;
        });
    }, [iconId, availIcons]);
    
    useEffect(() => {
        (async () => {
            try {
                const { data: { icons }} = await getInstance().get("/icons/all");
                setAvailIcons(icons);
                if (iconId) {
                    setSelIcon(icons.findIndex(x => x._id === iconId));
                }
            } catch (e) {
                await displaySwalError(e);
            }
        })()
    }, [iconId]);
    
    useEffect(() => {
        if (selIcon < 0 || !availIcons.length) return;
        setIcon(availIcons[selIcon]);
    }, [availIcons, selIcon, setIcon]);
    
    const handleDelete = async (e) => {
        e.preventDefault();
        const { value } = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        });
        if (value) {
            try {
                const axios = getInstance();
                const { data: icon } = await axios.delete("/icons/"+availIcons[selIcon]._id);
                setAvailIcons(availIcons => availIcons.filter(x => x._id !== icon._id));
                await Swal.fire(
                    "Deleted!",
                    "This icon has been deleted.",
                    "success"
                );
            } catch (e) {
                await displaySwalError(e);
            }
        }
    };
    
    const Icon = ({ icon, idx }) => {
        const isDev = process.env.NODE_ENV === "development";
        const href= `${isDev ? "http://localhost:3000" : window.location.origin}/${icon.filepath.slice(7)}`;
        
        const handleClick = (e) => {
            e.preventDefault();
            setSelIcon(idx);
        };
        
        return <img alt={"product icon"} onClick={handleClick} className={["icon", "interactable", selIcon === idx ? "active" : null].join(" ")} src={href}/>
    };
    
    const handleUpload = async (e) => {
        e.preventDefault();
        if (e.target.files.length === 0 || isUploading) return;
        if (e.target.files[0].size > 100000) {
            return Swal.fire(
                'Error',
                'File size was too big, must be no more than 100KB',
                'error'
            )
        }
        const formData = new FormData();
        formData.append("icon", e.target.files[0], e.target.files[0].name || "icon");
        setUploading(true);
        try {
            const axios = getInstance();
            const { status, data: { icon }} = await axios.post("/icons/new", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            if (status === 201) {
                setAvailIcons(icons => icons.concat(icon));
                await Swal.fire(
                    'Hooray!',
                    'The icon was successfully uploaded!',
                    'success'
                );
            } else {
                await Swal.fire(
                    'Oh oh!',
                    'The icon submitted was already found on the server',
                    'warning'
                );
            }
        } catch (e) {
            await displaySwalError(e);
        } finally {
            setUploading(false);
        }
    };
    
    return <div>
        <div className={"icons"}>
            {availIcons.map((x,i) => <Icon key={x._id} idx={i} icon={x}/>)}
        </div>
        <span>
            <Form.Label className={["uploader", isUploading ? "text-muted" : null].join(" ")} htmlFor={"icon"} column={0}>Upload Icon</Form.Label>
            <Form.Control hidden onChange={handleUpload} id={"icon"} name={"icon"} type="file"/>
        </span>
        {selIcon < 0 ? null : <span onClick={handleDelete} className={"deleter"}>Delete Icon</span>}
    </div>
}

export default IconPicker;
