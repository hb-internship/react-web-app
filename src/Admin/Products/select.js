import React, {useState, useEffect} from "react";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Button} from "react-bootstrap";
import {capitalizeAll, getInstance} from "../../utils";


const CategorySelector = (props) => {
    
    const [availCatagories, setAvailCatagories] = useState([]);
    const [selected, setSelected] = useState(props.categories || []);
    const [cIdx, setCIdx] = useState(0);
    
    const { setCategories } = props;
    
    
    useEffect(() => {
        (async () => {
            const { data: { categories }} = await getInstance().get("/categories");
            setAvailCatagories(categories);
        })();
    }, []);
    
    useEffect(() => {
        setCategories(selected);
    }, [setCategories, selected]);
    
    const handleChange = (e) => {
        e.preventDefault();
        setCIdx(parseInt(e.target.value));
    };
    
    const handleAdd = (e) => {
        e.preventDefault();
        setSelected(oldCategories => {
            const result = oldCategories.filter(x => x._id !== availCatagories[cIdx]._id);
            if (result.length !== oldCategories.length) {
                return oldCategories;
            }
            result.push(availCatagories[cIdx]);
            return result;
        });
    };
    
    const handleRemove = (e) => {
        e.preventDefault();
        setSelected(sel => sel.filter(c => c._id !== availCatagories[cIdx]._id));
    };
    
    return <Form.Group controlId="categorySelector">
        <Form.Label column={0}>Categories</Form.Label>
        <div className={"categories"}>{selected.map(x => capitalizeAll(x.name)).join(", ")}</div>
        <Row>
            <Col>
                <Form.Control as="select" name={"category"} onChange={handleChange}>
                    {availCatagories && availCatagories.map((x,j) => <option key={j} value={j}>{capitalizeAll(x.name)}</option>)}
                </Form.Control>
            </Col>
            <Col className={"text-center"} md={3}>
                <Button onClick={handleAdd} size={"sm"}>Add</Button>
            </Col>
            <Col className={"text-center"} md={3}>
                <Button onClick={handleRemove} size={"sm"}>Remove</Button>
            </Col>
        </Row>
    </Form.Group>
};

export default CategorySelector;
