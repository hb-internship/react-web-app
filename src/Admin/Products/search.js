import React, {useEffect, useState} from "react";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import {getInstance} from "../../utils";

let timer = null;

const submitRequest = async (query, results, cbProducts, cbResults) => {
    const { data: { products }} = await  getInstance().get("/products/search?q="+query);
    cbProducts(p => {
        if (results.length === 0) {
            cbResults([].concat(p));
        }
        return products;
    });
};

const ProductSearch = ({ setProducts }) => {
    const [ query, setQuery ] = useState("");
    const [ results, setResults] = useState( []);

    useEffect(() => {
        if (timer) {
            clearTimeout(timer);
        }

        if (!query) {
            setProducts([].concat(results));
            return;
        }

        timer = setTimeout(submitRequest, 350, query, results, setProducts, setResults);
    }, [query, results, setProducts]);



    const handleChange = (e) => setQuery(e.target.value);

    return  <InputGroup className="mb-3">
        <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Search</InputGroup.Text>
        </InputGroup.Prepend>
        <FormControl
            onChange={handleChange}
            placeholder="Type a product's name"
            aria-label="Product"
            aria-describedby="basic-addon1"
        />
    </InputGroup>
};

export default ProductSearch;