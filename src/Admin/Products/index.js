import React, {useEffect, useState} from 'react';
import {getInstance, costToString, displaySwalError, capitalizeFirst, getProductIcon} from "../../utils";
import './style.sass';


import {Container, Jumbotron, Button, Popover} from "react-bootstrap";
import {generateBreadcrumb} from "../index";
import Swal from "sweetalert2";

import ProductModal from "./modal";

import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import ProductSearch from "./search";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Switch from "react-switch";
import Pagination from "react-bootstrap/Pagination";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPen, faTimes, faEye, faDollarSign, faCogs} from '@fortawesome/free-solid-svg-icons'


const UpdatingPopover = React.forwardRef(
    ({ scheduleUpdate, children, ...props }, ref) => {
        useEffect(() => {
            scheduleUpdate();
        }, [children, scheduleUpdate]);
        return (
            <Popover ref={ref} {...props}>
                {children}
            </Popover>
        );
    },
);

function Products() {
    const [page, setPage] = useState(0);
    const [products, setProducts] = useState([]);
    const [creating, setCreating] = useState(false);
    const [count, setCount] = useState(0);
    const [pageSize, setPageSize] = useState(0);

    const Product = ({ product }) => {
        const [editing, setEditing] = useState(false);
        const [visible, setVisible] = useState(product ? product.visible : false);

        const maxChars = 40;

        useEffect(() => {
            if (visible === product.visible) {
                return;
            }
            (async () => {
                try {
                    await getInstance().put("/products/"+product._id, { visible });
                } catch (e) {
                    await displaySwalError(e);
                }
            })()
        }, [visible, product]);

        const handleEdit = () => {
            setEditing(editing => !editing);
        };
        
        const handleDelete = async () => {
            const { value } = await Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            });
            if (!value) return;
            try {
                const { data: { product: deleted }} = await getInstance().delete("/products/"+product._id);
                setProducts(products.filter(x => x._id !== deleted._id));
                Swal.fire(
                    "Deleted!",
                    "This product has been deleted.",
                    "success"
                );
            } catch (e) {
                displaySwalError(e);
            }
        };

        const handleVisibilityChange = async (e) => {
            setVisible(e);
        }

        const categories = product.categories.map(x => capitalizeFirst(x.name)).join(", ");
        const shouldCropCats = categories.length > maxChars;
        
        return <div>
            <span>{product.icon ?
                <img alt={`${product.name}'s icon`} className={"icon"} src={getProductIcon(product)}/>
                : null}
            </span>
            <span>{product.name}
            </span>
            <span>{costToString(product.cost)}</span>
            {shouldCropCats ? <OverlayTrigger trigger={"hover"} overlay={
                <UpdatingPopover id={"popover-products"}>{categories}</UpdatingPopover>
            }>
                <span>{categories.slice(0,maxChars) + "..."}</span>
            </OverlayTrigger> : <span>{categories}</span>}
            <span>
                <Switch className={"visibility"} onColor={"#157ffb"} offColor={"#6d757d"} onChange={handleVisibilityChange} checked={visible} />
            </span>
            <span className={"options"}>
                <Button onClick={handleEdit} size={"sm"}><FontAwesomeIcon icon={faPen}/></Button>
                <ProductModal setProducts={setProducts} show={editing} product={product} editing={true} onHide={() => setEditing(false)} />
                <Button variant={"secondary"} onClick={handleDelete} size={"sm"}><FontAwesomeIcon icon={faTimes} size={"lg"}/></Button>
            </span>
        </div>
    };
    

    
    useEffect(() => {
        (async () => {
            try {
                const { data: { products, pageSize, count }} = await getInstance().get("/products/all");
                setProducts(products);
                setCount(count);
                setPageSize(pageSize);
            } catch (e) {
                await displaySwalError(e);
            }
        })();
    }, []);

    const maxPages = Math.ceil( count / pageSize );
    
    const handleClick = (event) => {
        event.preventDefault();
        setCreating(true);
    };

    const handlePrevPage = () => {
        setPage(Math.max(page - 1, 0));
    };

    const handleNextPage = () => {
        setPage(Math.min(page + 1, Math.ceil(count / pageSize) - 1));
    };
    
    return (
        <div>
            {generateBreadcrumb()}
            <Container>
                <Jumbotron>
                    <h1>Products page</h1>
                </Jumbotron>
                <Row>
                    <Col md={6}>
                        <Button onClick={handleClick}>Add Product</Button>
                        <ProductModal setProducts={setProducts} show={creating} editing={false} onHide={() => setCreating(false)}/>
                        {maxPages > 1 && <Pagination>
                            <Pagination.Prev disabled={page === 0} onClick={handlePrevPage}/>
                            <Pagination.Item disabled>Page {page + 1} of {maxPages}</Pagination.Item>
                            <Pagination.Next disabled={page === (maxPages - 1)} onClick={handleNextPage}/>
                        </Pagination>}
                    </Col>
                    <Col md={6}>
                        <ProductSearch setProducts={setProducts}/>
                    </Col>
                </Row>
                <div className={"custom-table products-table"}>
                    <div className={"header"}>
                        <div>
                            <span/>
                            <span>Name</span>
                            <span><FontAwesomeIcon icon={faDollarSign}/></span>
                            <span>Categories</span>
                            <span><FontAwesomeIcon icon={faEye}/></span>
                            <span><FontAwesomeIcon icon={faCogs}/></span>
                        </div>
                    </div>
                    <div className={"body"}>
                        {products.map((x,i) => <Product key={x._id} product={x} idx={i}/>)}
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default Products;
