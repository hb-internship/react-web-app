import React, {useEffect, useState, Fragment} from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";

import "./style.sass"

const BadgeScanner = ({ show, onHide, setBadgeId, loading }) => {
    
    const [buffer, setBuffer] = useState([]);
    
    const handleKey = (e) => {
        e.persist();
        if (e.charCode === 13) {
            e.preventDefault();
            return setBadgeId(buffer.join(""));
        }
        setBuffer(buffer => {
            const copy = buffer.concat(e.key);
            if (copy.length > 5) {
                copy.shift();
            }
            return copy;
        });
    };
    const inputId = React.createRef();
    
    useEffect(() => {
        if (inputId.current) {
            inputId.current.focus();
        }
    }, [show, inputId]);
    
    return <Modal show={show} onHide={onHide}>
        <Modal.Body>
            <div className={"scanner-container"}>
                {loading ? <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner> : <Fragment>
                    <h4>Please scan your badge</h4>
                    <input className={"badge-id"} onKeyPress={handleKey} ref={inputId} type={"number"}/>
                </Fragment>}
            </div>
        </Modal.Body>
        
        <Modal.Footer>
            <Button variant="secondary" onClick={onHide}>Close</Button>
        </Modal.Footer>
    </Modal>;
};

export default BadgeScanner;
