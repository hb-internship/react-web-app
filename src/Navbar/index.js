import {Container} from "react-bootstrap";
import {Link} from "react-router-dom";
import {NavLink} from "react-router-dom";
import React from "react";

import Navbar from "react-bootstrap/Navbar";
import Logo from "./hugobosslogowhite.png";

import "./style.sass"
import {capitalizeAll} from "../utils";

const NavigationBar = ({user}) => {
    return <Navbar bg={"dark"} variant={"dark"}>
        <Container>
            <Navbar.Brand as={Link} to={"/"}>
                <img alt={"Hugo Boss logo"} className={"logo"} src={Logo}/>
            </Navbar.Brand>
            <Navbar.Toggle/>
            {user && user.fullName && <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    Signed in as: <span className={"username"}>{capitalizeAll(user.fullName)}<small><NavLink to="/logout">Logout?</NavLink></small></span>
                </Navbar.Text>
            </Navbar.Collapse>}
        </Container>
    </Navbar>
};

export default NavigationBar;
