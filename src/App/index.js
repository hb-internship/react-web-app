import React, {useEffect} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";


import Admin from "../Admin";
import Login from "../Login";
import Logout from "../Logout";
import Home from "../Home"
import Shop from "../Shop"
import Orders from "../Orders"
import NavigationBar from "../Navbar"
import NotFound from "../NotFound";
import {checkAuthenticated} from "../utils";


export default function App() {

    const [user, setUser] = React.useState(null);
    
    useEffect(() => {
        checkAuthenticated();
    }, []);
    
    useEffect(() => {
        if (localStorage.user && !user) {
            setUser(JSON.parse(localStorage.user));
        }
    }, [user, setUser]);
    
    
    return (
        <Router>
            <NavigationBar user={user}/>
            <Switch>
                <Route path="/login">
                    <Login user={user} setUser={setUser}/>
                </Route>
                <Route path="/shop">
                    <Shop/>
                </Route>
                <Route path="/admin">
                    <Admin/>
                </Route>
                <Route path="/orders">
                    <Orders/>
                </Route>
                <Route path="/logout">
                    <Logout setUser={setUser}/>
                </Route>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route>
                    <NotFound/>
                </Route>
            </Switch>
        </Router>
    );
}
