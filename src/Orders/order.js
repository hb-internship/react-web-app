import React, {Fragment} from "react";
import ListGroup from "react-bootstrap/ListGroup";

import "./style.sass"
import {capitalizeAll, costToString} from "../utils";
import TimeAgo from "react-timeago";

/*eslint react/prop-types:0*/
const Order = ({ order, properties, active, idx, setActive }) => {
    const handleClick = (e) => {
        e.preventDefault();
        setActive(idx);
    };

    const renderDate = (order) => {
        const date = new Date(order.createdAt);
        return <Fragment>
            <div className={"date"}>{date.toLocaleDateString()}</div>
            <div className={"time"}>{date.toLocaleTimeString()}</div>
        </Fragment>
    };

    const countProducts = (order) => {
        let count = 0;
        Object.keys(order.order).forEach(product => {
           count += order.order[product];
        });
        return count;
    };

    return <ListGroup.Item action onClick={handleClick} active={idx === active}>
        <div className={"order"}>
            <div className={"chrono"}>
                {renderDate(order)}
            </div>
            <div className={"info"}>
                <div className={"buyer"}>Buyer: {capitalizeAll(order.user.fullName)}</div>
                <div>
                    <div className={"products"}>
                        Products: {countProducts(order)}
                    </div>
                    <div className={"timeago"}>
                        <TimeAgo date={new Date(order.createdAt).getTime()}/>
                    </div>
                </div>
            </div>
            <div className={"cost"}>{costToString(order.cost)}</div>
        </div>
    </ListGroup.Item>
};

export default Order;