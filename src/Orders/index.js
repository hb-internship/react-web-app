/* eslint-disable react-hooks/exhaustive-deps */


import React, {useEffect, useState} from "react";
import {Container, Jumbotron} from "react-bootstrap";
import {getInstance} from "../utils";
import ListGroup from "react-bootstrap/ListGroup";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Order from "./order";
import OrderDetails from "./details";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import {generateBreadcrumb} from "../Admin";

const isDev = process.env.NODE_ENV === 'development';
const wssInstance = new WebSocket(isDev ? 'ws://localhost:3000' : `ws://${window.location.host}`);

let interval = null;
let timer = null;

const checkOrders = (time, orders, cbOrders) => {
    if (!orders) return;
    cbOrders(o => {
        const filtered = o.filter(order => {
            const d = new Date(order.createdAt);
            return (Date.now() - d.getTime()) < time*60*1000;
        });
        return filtered;
    });
};

export default function Orders() {
    const [wss] = useState(wssInstance);
    const [ orders, setOrders ] = useState([]);
    const [ products, setProducts ] = useState({});
    const [ timeframe, setTimeframe ] = useState(15);
    const [ active, setActive ] = useState(0);

    useEffect(() => {
        if (active >= orders.length) {
            setActive(orders.length - 1);
        }
    }, [orders, active]);

    useEffect(() => {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(async () => {
            const { data: { baskets, products }} = await getInstance().get("/orders?m="+timeframe);
            setOrders(baskets.reverse());
            setProducts(products);
        }, 500);

        if (interval) {
            clearInterval(interval);
        }
        interval = setInterval(checkOrders, 1000, timeframe, orders, setOrders);
    }, [timeframe]);


    useEffect(() => {
        wss.onopen = () => {
            console.log("WSS Connected");
        };
        wss.onmessage = (msg) => {
            const { basket, products: prods, user } = JSON.parse(msg.data);
            if (!basket || !prods || !user) {
                return;
            }
            basket.user = user;
            setProducts(ps => Object.assign({}, ps, prods));
            setOrders(os => {
                return [].concat(basket,os);
            });
        };
        wss.onclose = () => {
            console.log("WSS Closed");
        };
    }, [wss]);


    useEffect(() => {
        return () => {
            wss.close();
        }
    }, []);

    const handleTimeChange = (e) => {
        e.persist();
        setTimeframe(Math.min(Math.max(e.target.value, 1), 30));
    };
    
    return <div className={"orders"}>
        {generateBreadcrumb()}
        <Container>
            <Jumbotron>
                <h1>Orders</h1>
                <p>A list of the most recent orders</p>
            </Jumbotron>
            <Row>
                <Col md={4}>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon3">
                                Timeframe in minutes:
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl id="basic-url" aria-describedby="basic-addon3" value={timeframe} onChange={handleTimeChange} step={5} type={"number"} />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col md={7} className={"orders-list"}>
                    <ListGroup>
                        {
                            orders.length > 0 ?
                                orders.map((x,i) => <Order
                                    key={i}
                                    order={x}
                                    active={active}
                                    idx={i}
                                    setActive={setActive}
                                    products={products}/>
                                    ) : <span>No orders have been registered in the last {timeframe !== 1 && timeframe} minute{timeframe !== 1 && "s"}.</span>
                        }
                    </ListGroup>
                </Col>
                <Col md={5} className={"details"}>
                    <OrderDetails order={orders[active]} products={products}/>
                </Col>
            </Row>
        </Container>
    </div>;
};
