import React, {Fragment} from "react";
import {capitalizeAll, costToString, getProductIcon} from "../utils";

const OrderDetails = ({ order, products }) => {
    if (!order) {
        return null;
    }

    const renderDate = () => {
        const date = new Date(order.createdAt);
        return <Fragment>
            <div className={"date"}>Date: {date.toLocaleDateString()}</div>
            <div className={"time"}>Time: {date.toLocaleTimeString()}</div>
        </Fragment>
    };

    const renderProducts = () => {
        if (Object.keys(products).length === 0) return null;
        return order.products.map((id, i) => <div key={i} className={"product"}>
                {products[id].icon ? <img alt={"product's icon"} src={getProductIcon(products[id])}/> : <span/>}
                <span>{products[id].name}</span>
                <span>{order.order[id]} ×</span>
                <span>{costToString(products[id].cost)}</span>
            </div>
        );

    };

    return <div className={"selected-order"}>
        <div><h4>Order Details</h4></div>
        <hr/>
        <div className={"date"}>{renderDate()}</div>
        <hr/>
        <div><h5>{order.user.isGuest ? "Guest" : "Employee"}</h5></div>
        <div>
            <div>Full name: {capitalizeAll(order.user.fullName)}</div>
            <div>Username: {order.user.username}</div>
        </div>
        <hr/>
        <div><h5>Cart</h5></div>
        <div className={"order-products"}>
            {renderProducts()}
            <div className={"total-cost"}>Total: <span>{costToString(order.cost)}</span></div>
        </div>
    </div>
};

export default OrderDetails;