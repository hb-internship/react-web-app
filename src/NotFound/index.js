import React from "react";
import {Container, Jumbotron} from "react-bootstrap";

export default function NotFound() {
    return <div className={"not-found"}>
        <Container>
            <Jumbotron>
                <h1>Not found</h1>
            </Jumbotron>
        </Container>
    </div>
}
