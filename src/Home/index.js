import React from "react";
import {Container, Jumbotron} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import {generateBreadcrumb} from "../Admin";

import "./style.sass";
import {faUtensils} from "@fortawesome/free-solid-svg-icons/faUtensils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faConciergeBell} from "@fortawesome/free-solid-svg-icons/faConciergeBell";
import {faFileInvoiceDollar} from "@fortawesome/free-solid-svg-icons/faFileInvoiceDollar";

export default function Home() {
    return <div className={"homepage"}>
        {generateBreadcrumb()}
        <Container>
            <Jumbotron>
                <h1>Home page</h1>
            </Jumbotron>
            <div className={"lander"}>
                <NavLink className={"img-button"} to={"/shop"}>
                    <span className={"text"}>Bistro</span>
                    <span className={"icon"}><FontAwesomeIcon icon={faUtensils} color={"#157ffb"} size={"6x"}/></span>
                </NavLink>
                <NavLink className={"img-button"} to={"/orders"}>
                    <span className={"text"}>Recent Orders</span>
                    <span className={"icon"}><FontAwesomeIcon icon={faConciergeBell} color={"#157ffb"} size={"6x"}/></span>
                </NavLink>
                <NavLink className={"img-button"} to={"/admin"}>
                    <span className={"text"}>Accounting</span>
                    <span className={"icon"}><FontAwesomeIcon icon={faFileInvoiceDollar} color={"#157ffb"} size={"6x"}/></span>
                </NavLink>
            </div>
        </Container>
    </div>
};
